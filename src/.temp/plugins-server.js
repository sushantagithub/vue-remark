import plugin_gridsome_vue_remark_5 from "C:\\Users\\Sushanta\\Desktop\\Gridsome\\vue-remark\\node_modules\\@gridsome\\vue-remark\\gridsome.client.js"

export default [
  {
    run: plugin_gridsome_vue_remark_5,
    options: {"typeName":"Post","baseDir":"./content/posts","pathPrefix":"/posts","template":"./src/templates/Post.vue","remark":{"plugins":["@gridsome/remark-prismjs"]},"index":["index"],"includePaths":[],"plugins":[],"refs":{}}
  }
]
