export default [
  {
    path: "/posts/second-post/",
    component: () => import(/* webpackChunkName: "page--src-templates-post-vue" */ "C:\\Users\\Sushanta\\Desktop\\Gridsome\\vue-remark\\src\\templates\\Post.vue"),
    meta: {
      $vueRemark: () => import(/* webpackChunkName: "vue-remark--content-posts-second-post-md" */ "C:\\Users\\Sushanta\\Desktop\\Gridsome\\vue-remark\\content\\posts\\second-post.md")
    }
  },
  {
    path: "/posts/first-post/",
    component: () => import(/* webpackChunkName: "page--src-templates-post-vue" */ "C:\\Users\\Sushanta\\Desktop\\Gridsome\\vue-remark\\src\\templates\\Post.vue"),
    meta: {
      $vueRemark: () => import(/* webpackChunkName: "vue-remark--content-posts-first-post-md" */ "C:\\Users\\Sushanta\\Desktop\\Gridsome\\vue-remark\\content\\posts\\first-post.md")
    }
  },
  {
    path: "/about/",
    component: () => import(/* webpackChunkName: "page--src-pages-about-vue" */ "C:\\Users\\Sushanta\\Desktop\\Gridsome\\vue-remark\\src\\pages\\About.vue")
  },
  {
    name: "404",
    path: "/404/",
    component: () => import(/* webpackChunkName: "page--node-modules-gridsome-app-pages-404-vue" */ "C:\\Users\\Sushanta\\Desktop\\Gridsome\\vue-remark\\node_modules\\gridsome\\app\\pages\\404.vue")
  },
  {
    name: "home",
    path: "/",
    component: () => import(/* webpackChunkName: "page--src-pages-index-vue" */ "C:\\Users\\Sushanta\\Desktop\\Gridsome\\vue-remark\\src\\pages\\Index.vue")
  },
  {
    name: "*",
    path: "*",
    component: () => import(/* webpackChunkName: "page--node-modules-gridsome-app-pages-404-vue" */ "C:\\Users\\Sushanta\\Desktop\\Gridsome\\vue-remark\\node_modules\\gridsome\\app\\pages\\404.vue")
  }
]

